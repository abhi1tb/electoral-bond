import tabula
purchases = tabula.convert_into("data/purchase.pdf", "data/purchase.csv", output_format="csv", pages='all')
redeems = tabula.convert_into("data/redeem.pdf", "data/redeem.csv", output_format="csv", pages='all')
