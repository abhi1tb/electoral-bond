# Electoral Bond

Simple python scripts to create a JSON out of the data published by the Election Commission of India regarding electoral bonds.
Source :- https://www.eci.gov.in/disclosure-of-electoral-bonds

# Dependencies
 - python3
 - openjdk
 - tabula-py

# Scripts
 - csv_generator.py - generates CSV from PDF data in the `data` directory.
 - bond.py - converts CSV into JSON with meaningful data.
 
# Data
All data are present in the `data` directory.
