import csv, json

redeemData = {}
purchaseData = {}
with open('data/redeem.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        party = row[2].strip()
        bond_number = row[5].strip()
        if party not in redeemData.keys():
            redeemData[party] = [bond_number]
        else:
            redeemData[party].append(bond_number)

    
with open('data/purchase.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    purchaseData = {}
    for row in csv_reader:
        status = row[11].strip()
        purchaser = row[5].strip().replace("  ","")
        bond_number = row[7].strip()
        denomination = row[8].strip()
        if status == "Paid":
            if purchaser not in purchaseData.keys():
                purchaseData[purchaser] = {}
            for party in redeemData:
                if bond_number in redeemData[party]:
                    if party in purchaseData[purchaser].keys():
                        purchaseData[purchaser][party] += float(denomination.replace(",",""))
                    else:
                        purchaseData[purchaser][party] = float(denomination.replace(",",""))
with open('data/donations.json', 'w') as json_file:
    json_file.write(json.dumps(purchaseData))
